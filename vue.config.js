module.exports = {
  pwa: {
    name: "FakeMoney",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black-translucent",
    themeColor: "#eac420",
    msTileColor: "#eac420"
  },

  publicPath: "/fake-money",
  outputDir: undefined,
  assetsDir: "assets",
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined,
  css: undefined
};
