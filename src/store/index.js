import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
import actions from "./actions";

Vue.use(Vuex);

const state = {
  prices: JSON.parse(localStorage.getItem("prices")) || []
};

const mutations = {
  addPrice(state, price) {
    state.prices.push(price);
    localStorage.setItem("prices", JSON.stringify(state.prices));
  },
  removePrice(state, price) {
    let index = state.prices.indexOf(price);
    setTimeout(() => {
      state.prices.splice(index, 1);
      localStorage.setItem("prices", JSON.stringify(state.prices));
    }, 200);
  },
  deleteAllPrices(state) {
    if (confirm("Sure?")) {
      state.prices = [];
      localStorage.removeItem("prices");
    }
  }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});
