export default {
  addPrice(context) {
    context.commit("addPrice");
  },
  removePrice(context, price) {
    context.commit("removePrice", price);
  },
  deleteAllPrices(context) {
    context.commit("deleteAllPrices");
  }
};
